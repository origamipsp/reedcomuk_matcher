# matcher reed.com.uk

## API
1) POST '/api/matcher/addCompanies' *- accepts an array of companies*  
*example:*  

{  

    "companies" : ["experis-ltd/p24927", ...]  

}  

*Collects data about the company and its vacancies*
___________________________________________________________
2) GET '/api/matcher/findScammers' *- launches a search for scammers*
___________________________________________________________

3) POST '/api/matcher/getResults' *- takes the company url, not all companies have their own page, so we pass it like this*  

*example:*  

{  

    "url" : "/jobs/experis-ltd/p24927"  

}  

*returns result like this*

{  

    "results": [  

        {  
            "clientName": "Experis LTD",  
            "clientUrl": "/jobs/experis-ltd/p24927",  
            "parentVacancyUrl": "https://www.reed.co.uk/jobs/senior-creative-lead/47204906",  
            "scammerVacancyUrl": "https://www.reed.co.uk/jobs/freelance-senior-creative-lead-4-month-ftc/47216888?source=searchResults&filter=%2fjobs%2fsenior-creative-lead-jobs",  
            "scammerCompanyName": "Creative Recruitment"  

        }  

    ]  

}  



