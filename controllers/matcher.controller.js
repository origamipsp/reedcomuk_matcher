const { StaticPool } = require('node-worker-threads-pool')
const os = require('os')
const db = require('../models/index.js')
//const fs = require('fs')
const path = require('path')
const axios = require('axios')
const cheerio = require('cheerio')
const { Op } = require("sequelize");
//const { table } = require('console')
const c = require('ansi-colors');


const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer

const min = 100
const max = 195

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

//путь к воркеру
const filePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_reed.js');
const numCPUs = os.cpus().length

//получаем количество страниц для парсинга
async function getPagesCount(agency) {
  try {
    const html = await axios.get(`${agency}`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    return pagesCount

  } catch (error) {
    console.log(error)
  }
}

//парсим url всех вакансий нужной компании, закидываем их в массив
async function addCompanies(req, res) {
  try {
    console.log(req.body.companies)
    let urlList = []
    let agencyArr = req.body.companies
    for (let j = 0; j < agencyArr.length; j++) {
      //await parseCompanyName(agencyArr[j]) // вынесен парсинг названия компании и запись в бд
      const pageCount = await getPagesCount(agencyArr[j])
      for (let i = 1; i <= pageCount; i++) {
        try {// https://www.reed.co.uk/jobs/amazon/p2630?pageno=2&sortby=DisplayDate
          const html = await axios.get(`${agencyArr[j]}?pageno=${i}`)
          console.log('add - ', agencyArr[j])
          const $ = cheerio.load(html.data)
          $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {
            let cutUrl = `${$(elem).attr('href')}`
            //console.log("!!!!!!!!cutUrl- ", cutUrl)
            let symbol = cutUrl.indexOf("?")
            cutUrl = cutUrl.substring(0, symbol)
            urlList.push(cutUrl)
          })
          let companyName = ""
          $('div > div.col-sm-12.col-md-9.details > header > div.job-result-heading__posted-by > a').each((i, elem) => {
            companyName = `${$(elem).text()}`
          })
          const [company, created] = await Company.findOrCreate({
            where: { url: agencyArr[j] },
            defaults: { companyName, url: agencyArr[j] }
          });
        }
        catch (e) {
          console.log(e)
        }
      }
    }
    //await parseAll(urlList) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
    res.status(200).json({ message: 'ok' })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

async function deleteCompanies(req, res) {
  for (let i = 0; i < req.body.companies.length; i++) {
    try {
      let toDelete = await Company.findOne({
        where: { url: req.body.companies[i] },
      })
      toDelete.destroy()
    } catch (error) {
      console.log(c.red(error));
    }
  }
  res.status(200).json("ALL DONE")
}

// считываем все с бд и передаем параметры в воркеры
async function findScammers(req, res) {

  if (!+process.env.WORKING) {



    res.status(200).json({ message: 'Started' })
    Vacancy.destroy({ truncate: { cascade: true } });
    Scammer.destroy({ truncate: { cascade: true } });
    //создаем пулл воркеров
    const pool = new StaticPool({
      size: 2,
      task: filePath,
      workerData: 'workerData!'
    });


    process.env.WORKING = 1
    const start = new Date().getTime();
    //const vacancies = await Vacancy.findAll()
    const companies = await Company.findAll()
    // let companiesNames = []
    let counter = 0;
    // for (let j = 0; j < companies.length; j++) {
    //   companiesNames.push(companies[j].dataValues.companyName)
    // }
    for (let i = 0; i < companies.length; i++) {
      (async () => {
        // https://www.reed.co.uk/jobs

        let location = companies[i].url.slice(20, 22)
        const result = await pool.exec({ url: companies[i].url, id: companies[i].id, cmpName: companies[i].companyName, location: location });
        counter++;
        if (counter >= companies.length - 1) {
          await pool.destroy()
          process.env.WORKING = 0
          const end = new Date().getTime();
          console.log(c.green(`END TIME: ${end - start}ms`));
        }
      })();
    }
  } else if (process.env.WORKING) {
    res.status(200).json({ message: "Working..." })
  }
}



//получаем результаты по post запросу в котором передаем url компании и возвращаем массив обьектов
async function getResults(req, res) {
  try {
    const result = await Company.findAll({
      //where: { url: req.body.url }, //TODO
      include: [{
        model: Vacancy,
        include: [{
          model: Scammer,
        }]
      }]
    })
    let results = []
    for (let i = 0; i < result.length; i++) {
      for (let j = 0; j < result[i].dataValues.table_vacancies.length; j++) {
        for (let k = 0; k < result[i].dataValues.table_vacancies[j].dataValues.table_scammers.length; k++) {
          if (typeof result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k] !== 'undefined') {
            results.push(
              {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyName": result[i].dataValues.table_vacancies[j].dataValues.title,//
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "parentVacancyCity": result[i].dataValues.table_vacancies[j].dataValues.city, //
                "parentCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.logo, //
                "scammerVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.url,
                "scammerCompanyName": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.companyName,
                "scammerJobTitle": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.jobTitle,
                "scammerJobCity": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.city,//
                "scammerCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.logo,//
              }
            ) 
            try {
              const post = await axios.post('https://matcher2.bubbleapps.io/api/1.1/wf/matcher_reed', {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyName": result[i].dataValues.table_vacancies[j].dataValues.title,//
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "parentVacancyCity": result[i].dataValues.table_vacancies[j].dataValues.city, //
                "parentCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.logo, //
                "scammerVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.url,
                "scammerCompanyName": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.companyName,
                "scammerJobTitle": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.jobTitle,
                "scammerJobCity": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.city,//
                "scammerCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.logo,//
              })
            } catch (error) {
              logger.error(`Error axios.post to https://matcher2.bubbleapps.io/api/1.1/wf/matcher_reed error: ${e}`)
            }


            try {
              const post = await axios.post('https://hrprime.bubbleapps.io/api/1.1/wf/matcher_reed', {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyName": result[i].dataValues.table_vacancies[j].dataValues.title,//
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "parentVacancyCity": result[i].dataValues.table_vacancies[j].dataValues.city, //
                "parentCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.logo, //
                "scammerVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.url,
                "scammerCompanyName": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.companyName,
                "scammerJobTitle": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.jobTitle,
                "scammerJobCity": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.city,//
                "scammerCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.logo,//
              })
            } catch (error) {
              logger.error(`Error axios.post to https://hrprime.bubbleapps.io/api/1.1/wf/matcher_reed error: ${e}`)
            }


          }
        }
      }
    }




    //res.status(200).json({ results })
    res.status(200).json("ALL DONE")
  } catch (error) {
    //res.status(500).json({ message: error.message })
  }
}

module.exports = {
  addCompanies, findScammers, getResults, deleteCompanies
}
