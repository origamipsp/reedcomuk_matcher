module.exports = (sequelize, Sequelize) => {
    const Company = sequelize.define("table_companies", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        companyName: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.STRING
        },
       
    },
    {
        indexes: [
            // Create a unique index on email
            {
                unique: false,
                fields: ['url']
            }
        ]
    })

    return Company
}