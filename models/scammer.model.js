module.exports = (sequelize, Sequelize) => {
    const Scammer = sequelize.define("table_scammers", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        vacancyId: {
            type: Sequelize.INTEGER,
        },

        url: {
            type: Sequelize.STRING
        },
        companyName: {
            type: Sequelize.STRING
        },

        jobTitle: {
            type: Sequelize.STRING
        },

        city: {
            type: Sequelize.STRING(500)
        },

        logo: {
            type: Sequelize.STRING(2000)
        },
    },
        {
            indexes: [
                // Create a unique index on email
                {
                    unique: true,
                    fields: ['url']
                }
            ]
        })

    return Scammer
}