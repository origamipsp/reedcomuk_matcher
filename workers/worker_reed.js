const { parentPort } = require('worker_threads')
const axios = require('axios')
const cheerio = require('cheerio')
const { distance } = require('fastest-levenshtein')
const db = require('../models/index.js')
const { Op } = require("sequelize");
const c = require('ansi-colors');
const log = require('simple-node-logger')
const { green } = require('ansi-colors')
const logger = log.createSimpleFileLogger('project.log')

const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer
const min = 100
const max = 195
//const locations = ['DE', 'FR', 'FI', 'SE', 'IE', 'HK', 'NL', 'SG', 'GB', 'CA', 'AT', 'ES', 'CZ', 'AE']
const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// получаем количество страниц потенциальных скамеров, удовлетворяющих поиску по тайтлу искомой вакансии заказчика
async function getPagesCountParent(url) {
  await delay(getRandomInt(min, max))
  try {
    //const html = await axios.get(`${url}-jobs`)
    const html = await axios.get(`${url}?datecreatedoffset=LastThreeDays`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    //console.log(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    //console.log(pagesCount)
    return pagesCount

  } catch (error) {
    console.log(`getPagesCount error: ${error.message}`)
  }
}


async function doAll({ url, id, cmpName, location }) {
  console.log(location)
  console.log(c.green("Worker is working..."))
  let idList = []
  let pageCount = await getPagesCountParent(url)
  for (let i = 0; i < pageCount; i++) {
    idList = idList.concat(await getUrlsFromPage(url, i,))
    console.log("VACANCY TOTAL: ", idList.length)
  }
  let arr = [...new Set(idList)]
  console.log(c.green(`UNIQ VACANCY TOTAL: ${arr.length}`))
  console.log(arr)
  await parseAll(arr, id, location, cmpName, url) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
}

async function getUrlsFromPage(agency, i) {

  let idList = []
  // do {
  //await delay(getRandomInt(min, max))
  try {
    console.log("PARSING PAGE №: ", i + 1)
    const html = await axios.get(`${agency}?pageno=${i}&sortby=DisplayDate&datecreatedoffset=LastThreeDays`)
    const $ = cheerio.load(html.data)
    $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {
      let vacancyId = `${$(elem).attr('href')}`
      //var cutId = vacancyId.substring(vacancyId.indexOf(',') + 1, vacancyId.lastIndexOf(','));
      idList.push(vacancyId)
    })
    console.log("VACANCY COUNT PARSED ON PAGE №: ", i + 1, " IS: ", idList.length)
  }
  catch (e) {
    //console.log(c.red("getUrlsFromPage error:", e))
    //logger.error(`getUrlsFromPage error:  ${e}`)
  }
  // } while (idList.length == 0);
  console.log("VACANCY FROM PAGE: ", idList.length)
  //console.log("UNIQ VACANCY FROM PAGE: " ,[...new Set(idList)].length)
  return idList
}


//парсим всё нужное нам содержимое из каждой url массива
async function parseAll(idList, companyId, location, cmpName, cmpUrl) {

  for (let i = 0; i < idList.length; i++) {

    try {
      const html = await axios.get(`https://www.reed.co.${location}${idList[i]}`)
      const $ = cheerio.load(html.data)

      let title = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })

      let titleForParsing // это идёт в парсинг
      try {
        titleForParsing = title.replace(/[^a-zа-яё0-9-\s]/gi, '').split(' ').filter(Boolean).join('%20') // тонкости языка, тире заменено на %20, тайтл заменен на titleForParsing
        //titleForParsing = titleForParsing.split('/').filter(Boolean).join('%2F') // тонкости языка, тире заменено на %20, тайтл заменен на titleForParsing
      } catch (error) {
        titleForParsing = "" // тайтл заменен на titleForParsing
        console.log("title error", error)
        logger.error(`title error:  ${error}`)
      }

      //Удаляем мусор из тайтла и приводим его к необходимому виду
      // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
      //title = title.replace(/[^a-zа-яё0-9\s]/gi, '').split(' ').filter(Boolean).join('-')  // A!TENTION

      let description = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > div.description-container.row > div > div.description > span').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      description = description.split(' ').join('')
      if (description.length >= 499) {
        description = description.substring(0, 499)
      }

      // let companyName = ""
      // $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a > span').each((i, elem) => {
      //   companyName += `${$(elem).text()}`
      // })

      // let companyUrl = ""
      // $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a').each((i, elem) => {
      //   companyUrl += `${$(elem).attr('href')}`
      // })

      let city = ""
      $('div.hidden-xs > div > div.location.col-xs-12.col-sm-6.col-md-6.col-lg-6 > span:nth-child(3) > a > span > span').each((i, elem) => {
        city += `${$(elem).text()}`

      })

      let logo = ""
      $('.logo-profile.hidden-xs.text-center > a.logo-wrap.logo-wrap--border-bottom > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`
      })

      const [vacancy, created2] = await Vacancy.findOrCreate({
        where: {
          url: "" + idList[i]
        },
        defaults: { companyId, title, url: `https://www.reed.co.${location}${idList[i]}`, description, city, logo, companyName: cmpName, companyUrl: cmpUrl }
      });

      console.log(c.green("VACANCY SAVED"))
      await parseJobsUrl({
        id: vacancy.id, title: titleForParsing, url: idList[i],   // тайтл заменен на titleForParsing
        description: description,
        city: city, logo: logo,
        cmpName: cmpName,
        cmpUrl: cmpUrl,
      }, location)
    } catch (e) {
      console.log(c.red(`VACANCY NOT SAVED error: ${e}`))
      logger.error(`VACANCY NOT SAVED error:  ${e}`)
    }
  }
}



// получаем количество страниц потенциальных скамеров, удовлетворяющих поиску по тайтлу искомой вакансии заказчика
async function getPagesCount(title, location) {
  await delay(getRandomInt(min, max))
  try {
    const html = await axios.get(`https://www.reed.co.${location}/jobs/${title}-jobs`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`

    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    //console.log(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    //console.log(pagesCount)
    return pagesCount
  } catch (error) {
    console.log(`getPagesCount error: ${error.message}`)
  }
}

//парсим url всех потенциальных скамеров, закидываем их в массив  obj = 1 вакансия
async function parseJobsUrl(obj, location) {
  //progress.setHeader('Working ...').start();
  const pageCount = await getPagesCount(obj.title, location)
  let potentialScammerList = []
  for (let i = 1; i <= pageCount; i++) {
    try {
      await delay(getRandomInt(min, max))
      const html = await axios.get(`https://www.reed.co.${location}/jobs/${obj.title}-jobs?pageno=${i}`)
      const $ = cheerio.load(html.data)
      //let cutUrl = "" //???
      $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {
        let cutUrl = `https://www.reed.co.${location}${$(elem).attr('href')}` //????????????
        //cutUrl += `${$(elem).attr('href')}` //??????
        let symbol = cutUrl.indexOf("?")
        cutUrl = cutUrl.substring(0, symbol)
        if (obj.url !== cutUrl) {
          potentialScammerList.push(cutUrl)
        }
      })
    } catch (error) {
      console.log(`ParseJobsUrl error: ${error.message}`)
    }
  }
  await parseAllFrompotentialScammer(potentialScammerList, obj)
}

//парсим название компании и записываем в бд
async function parseCompanyName(agency) {
  console.log(agency)
  try {
    const html = await axios.get(`https://www.reed.co.uk/company-profile/${agency}`)
    const $ = cheerio.load(html.data)
    let companyName = ""
    $('div:nth-child(2) > div > section.profile__information > h1 > span').each((i, elem) => {
      companyName += `${$(elem).text()}`
    })
    companyName = companyName.replace("Working at ", "")
    const [company, created] = await Company.findOrCreate({
      where: { url: agencyArr },
      defaults: { companyName, url: agencyArr }
    });
  } catch (error) {
    console.log(error)
  }
}

//парсим всё нужное нам содержимое  из каждой url массива потенциальных скамеров
async function parseAllFrompotentialScammer(urls, obj) {

  for (let i = 0; i < urls.length; i++) {
    try {
      await delay(getRandomInt(min, max + 350))
      const html = await axios.get(urls[i])
      const $ = cheerio.load(html.data)

      let title = ""
      $('#content > div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })
      console.log(c.yellow(`TITLE:  ${title}`))
      let description = ""
      $('#content > div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > div.description-container.row > div > div.description > span').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      description = description.split(" ").join('')
      if (description.length >= 499) {
        description = description.substring(0, 499)
      }
      console.log(c.green(`DESCRIPTION: ${description.length}`))

      let companyName = ""
      $('#content > div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a > span').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })
      console.log(c.yellow(`COMPANY NAME: ${companyName}`))

      let companyUrl = ""
      $('#content > div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })
      console.log(c.yellow(`COMPANY URL: ${companyUrl}`))

      let city = ""
      $('.location.col-xs-12.col-sm-6.col-md-6.col-lg-6 > span:nth-child(3) > a > span > span').each((i, elem) => {
        city += `${$(elem).text()}`
      })
      console.log(c.yellow(`CITY: ${city}`))

      let logo = ""
      $('.logo-wrap.logo-wrap--border-bottom > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`
      })
      console.log(c.yellow(`LOGO: ${logo}`))

      // проверяем расстояние левинштайна между описанием вакансси заказчика и описанием каждого потенциального заказчика
      // если удовлетворяет условию - записываем в бд найденного скамера
      let diff = distance(obj.description, description) / obj.description.length
      // console.log("includes___________: ", obj.companies.includes(companyName))
      // console.log("scammerName________: ", companyName)
      // console.log("companyName________: ", obj.companies)
      let agencyCheck = await Company.findOne({ where: { companyName: companyName } })
      if (diff < 0.3 && !obj.cmpName.includes(companyName) && !agencyCheck) {
        console.log(c.red('SCAMMER!!!!!!!!!!!!!!!!!!!!!!!!!!'));
        const [scammer, created] = await Scammer.findOrCreate({
          where: {
            //url: urls[i]
            [Op.or]: [
              { url: urls[i] },
              { companyName: companyName }
            ]
          },
          //defaults: { vacancyId: obj.id, url: urls[i], companyName }
          defaults: { vacancyId: obj.id, url: urls[i], companyName, jobTitle: title, city, logo }
        });

        if (process.env.NODE_ENV === 'production') {
          try {
            const post = await axios.post('https://matcher2.bubbleapps.io/api/1.1/wf/matcher_reed', {
              "clientName": obj.cmpName,
              "clientUrl": obj.cmpUrl,
              "parentVacancyName": obj.title,//
              "parentVacancyUrl": `https://www.reed.co.${location}${obj.url}`,
              "parentVacancyCity": obj.city, //
              "parentCompanyLogo": obj.logo, //

              "scammerVacancyUrl": urls[i],
              "scammerCompanyName": companyName,
              "scammerJobTitle": title,
              "scammerJobCity": city,//
              "scammerCompanyLogo": logo,//
            })
          } catch (error) {
            logger.error(`Error axios.post to https://matcher2.bubbleapps.io/api/1.1/wf/matcher_reed error: ${e}`)
          }



          try {
            const post = await axios.post('https://hrprime.bubbleapps.io/api/1.1/wf/matcher_reed', {
              "clientName": obj.cmpName,
              "clientUrl": obj.cmpUrl,
              "parentVacancyName": obj.title,//
              "parentVacancyUrl": `https://www.reed.co.${location}${obj.url}`,
              "parentVacancyCity": obj.city, //
              "parentCompanyLogo": obj.logo, //

              "scammerVacancyUrl": urls[i],
              "scammerCompanyName": companyName,
              "scammerJobTitle": title,
              "scammerJobCity": city,//
              "scammerCompanyLogo": logo,//
            })
          } catch (error) {
            logger.error(`Error axios.post to https://hrprime.bubbleapps.io/api/1.1/wf/matcher_reed error: ${e}`)
          }





        }
      }
    }
    catch (e) {
      console.log(`parseAllFrompotentialScammer error: ${e.message}`)
    }
  }
}

// Основной поток передаст нужные вам данные
// через этот прослушиватель событий.
parentPort.on('message', async (param) => {
  const result = await doAll(param);
  // Access the workerData.
  // return the result to main thread.
  parentPort.postMessage("feels good");
});
